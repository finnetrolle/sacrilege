package ru.trollsmedjan.dao.model.autocompleter;

import java.util.*;

/**
 * Created by syachin on 22.12.2014.
 */
public class AutoCompleteNode {

    private final char letter;
    private AutoCompleteNode parent;
    private String word = null;
    private Map<Character, AutoCompleteNode> childs = null;
    private int returns = 0;
    private static int queries = 0;

    public static void incrementQueries() {
        queries++;
    }

    public AutoCompleteNode(char letter, AutoCompleteNode parent) {
        this.letter = Character.toLowerCase(letter);
//        this.letter = letter;
        this.parent = parent;
    }

    private void setWord(String word) {
        this.word = word;
    }

    public void addWord(String word) {
        addWord(0, word);
    }

    private void addWord(int index, String word) {
        if (index < word.length()) {
            AutoCompleteNode child = addCharacter(word.charAt(index));
            if (index + 1 == word.length()) {
                child.setWord(word);
            } else {
                child.addWord(index + 1, word);
            }
        }
    }

    private AutoCompleteNode addCharacter(Character cc) {
        Character c = Character.toLowerCase(cc);
        if (childs == null) childs = new HashMap<Character, AutoCompleteNode>();
        if (childs.containsKey(c))
            return childs.get(c);
        AutoCompleteNode node = new AutoCompleteNode(c, this);
        childs.put(c, node);
        return node;
    }

//    public AutoCompleteNode addEndingCharacter(Character c, String word) {
//        AutoCompleteNode node = addCharacter(c);
//        node.setWord(word);
//        return node;
//    }

    public String getWord() {
        AutoCompleteNode.incrementQueries();
        returns++;
        return this.word;
    }

    public String getWordForced() {
        return getWordForced(this, new StringBuilder()).toString();
    }

    private static StringBuilder getWordForced(AutoCompleteNode node, StringBuilder builder) {
        if (node.parent == null) { // here is the root
            return builder;
        } else {
            builder.insert(0, node.letter);
            return getWordForced(node.parent, builder);
        }
    }

    public List<String> getSuggestionList(int maxCount) {
        List<String> list = new LinkedList<String>();
        getSuggestionList(maxCount, list);
        return list;
    }

    private void getSuggestionList(int maxCount, List<String> list) {
        if (maxCount <= list.size())
            return;

        if (this.word != null) {
            list.add(word);
        }
        if (this.childs != null) {
            for (AutoCompleteNode node : childs.values()) {
                node.getSuggestionList(maxCount, list);
            }
        }
    }

    public AutoCompleteNode getNodeForString(String string) {
        return this.getNodeForString(string, -1);
    }

    private AutoCompleteNode getNodeForString(String string, int index) {
        if (this.childs == null)
            return null;
        if (this.childs.containsKey(string.charAt(index+1))) {
            AutoCompleteNode child = childs.get(string.charAt(index + 1));
            if (index + 2 == string.length()) {
                return child;
            }
            return child.getNodeForString(string, index + 1);
        }
        return null;
    }

    public void killMe() {
        if (this.childs != null) {
            for (AutoCompleteNode child : childs.values()) {
                child.killMe();
            }
            this.childs.clear();
        }
        this.word = null;
        this.parent = null;
    }

}
