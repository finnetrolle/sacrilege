package ru.trollsmedjan.dao.model.autocompleter;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by syachin on 23.12.2014.
 */
public class AutoCompleter {

    public static long SELF_DESTROY_TIME_MSEC = 1000 * 60 ; // 10 minute
    private int loaded = 0;

    private AutoCompleteNode root = null;
    private long lastAccessTimestamp = System.currentTimeMillis();

    public int getLoaded() {
        return loaded;
    }

    public AutoCompleter() {
        lastAccessTimestamp = System.currentTimeMillis();
        this.root = new AutoCompleteNode('*', null);
    }

    public void addWord(String word) {
        lastAccessTimestamp = System.currentTimeMillis();
//        root.addWord(word.toLowerCase());
        root.addWord(word);
        loaded++;
    }

    public List<String> getWords(String word, int maxCount) {
        lastAccessTimestamp = System.currentTimeMillis();
//        AutoCompleteNode node = root.getNodeForString(word.toLowerCase());
        AutoCompleteNode node = root.getNodeForString(word);
        if (node != null) {
            return node.getSuggestionList(maxCount);
        }
        return new LinkedList<String>();
    }

    public AutoCompleter checkLifetime() {
        long now = System.currentTimeMillis();
        if (lastAccessTimestamp + SELF_DESTROY_TIME_MSEC <= now) {
            this.root.killMe();
            this.root = null;
            return null;
        }
        return this;
    }


}
