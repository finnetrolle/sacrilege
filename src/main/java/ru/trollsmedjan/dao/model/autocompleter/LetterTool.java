package ru.trollsmedjan.dao.model.autocompleter;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by syachin on 13.01.2015.
 */
public class LetterTool {

    final static Logger log = Logger.getLogger(LetterTool.class);
    private static LetterTool instance = null;
    private LetterTool() {
        fillVariations();
    }

    public static LetterTool getInstance() {
        if (instance == null)
            synchronized (LetterTool.class) {
                if (instance == null) {
                    instance = new LetterTool();
                    log.info("Letter tool initialized");
                }
            }
        return instance;
    }

    private Map<Character, List<Character> > variations = new HashMap<Character, List<Character>>();

    private List<Character> fillVariation(Character ... chars) {
        List<Character> result = new LinkedList<Character>();
        for (char c : chars) {
            result.add(c);
        }
        return result;
    }

    private void fillVariations() {
        variations.put('ф', fillVariation('q'));
        variations.put('ц', fillVariation('w'));
        variations.put('у', fillVariation('e'));
        variations.put('к', fillVariation('r'));
        variations.put('е', fillVariation('t'));
        variations.put('н', fillVariation('y'));
        variations.put('г', fillVariation('u'));
        variations.put('ш', fillVariation('i'));
        variations.put('щ', fillVariation('o'));
        variations.put('з', fillVariation('p'));
        variations.put('х', fillVariation('['));
        variations.put('ъ', fillVariation(']'));
        variations.put('ф', fillVariation('a'));
        variations.put('ы', fillVariation('s'));
        variations.put('в', fillVariation('d'));
        variations.put('а', fillVariation('f'));
        variations.put('п', fillVariation('g'));
        variations.put('р', fillVariation('h'));
        variations.put('о', fillVariation('j'));
        variations.put('л', fillVariation('k'));
        variations.put('д', fillVariation('l'));
        variations.put('ж', fillVariation(';'));
        variations.put('э', fillVariation('\''));
        variations.put('я', fillVariation('z'));
        variations.put('ч', fillVariation('x'));
        variations.put('с', fillVariation('c'));
        variations.put('м', fillVariation('v'));
        variations.put('и', fillVariation('b'));
        variations.put('т', fillVariation('n'));
        variations.put('ь', fillVariation('m'));
        variations.put('б', fillVariation(','));
        variations.put('ю', fillVariation('.'));
        variations.put('.', fillVariation('/'));
    }

    public boolean compareLetters(char inputLetter, char letter) {
        if (inputLetter == letter)
            return true;
        List<Character> vars = variations.get(inputLetter);
        if (vars != null) {
            for (char c : vars) {
                if (c == inputLetter)
                    return true;
            }
        }
        return false;
    }
}
