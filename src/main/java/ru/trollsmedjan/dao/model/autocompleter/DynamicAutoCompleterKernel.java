package ru.trollsmedjan.dao.model.autocompleter;

import org.apache.log4j.Logger;
import ru.trollsmedjan.dao.model.AddressTree.AddressNode;
import ru.trollsmedjan.dao.model.AddressTree.helpers.PerfomanceAnalyzer;
import ru.trollsmedjan.dao.model.House;
import ru.trollsmedjan.sacrilege.Sacrilege;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by finnetrolle on 24.12.2014.
 */
public class DynamicAutoCompleterKernel {

    private final Map<UUID, AutoCompleter> autoCompleters = new ConcurrentHashMap<UUID, AutoCompleter>();
    private int treesCreated = 0;
    private int treesDestroyed = 0;
    private int treesAvailable = 0;
    private int queriesProcessed = 0;
    private boolean silent = true;
    final static Logger log = Logger.getLogger(DynamicAutoCompleterKernel.class);

    private PerfomanceAnalyzer creationPA = new PerfomanceAnalyzer("DACK creation");
    private PerfomanceAnalyzer cleaningPA = new PerfomanceAnalyzer("DACK cleaning");
    private PerfomanceAnalyzer queryingPA = new PerfomanceAnalyzer("DACK querying");

    public int getTreesCreated() {
        return treesCreated;
    }

    public int getTreesDestroyed() {
        return treesDestroyed;
    }

    public int getTreesAvailable() {
        return treesAvailable;
    }

    public int getQueriesProcessed() {
        return queriesProcessed;
    }

    public DynamicAutoCompleterKernel setSilentMode(boolean silentMode) {
        silent = silentMode;
        return this;
    }


    private AutoCompleter registerAutoCompleter(AddressNode node) {
        // there are two types of autocompleter^
        // 1 - AO auto
        // 2 - House auto
        // eventually, we can have 3 type - mixed
        AutoCompleter auto = new AutoCompleter();
        // let's make first type
        List<String> toAdd = new LinkedList<String>();
        node.getSubTreeWords(toAdd);
//        if (node.hasChilds())
//            for (AddressNode n : node.getChildren()) {
//                auto.addWord(n.getName());
//            }
        // add houses - second type
        for (String s : toAdd)
            auto.addWord(s);
        if (node.hasHouses()) {
            List<House> list = Sacrilege.getInstance().getDao().getHouses(node.getUUID());
            for (House house : list)
                auto.addWord(house.getNumber());
        }
        treesCreated++;
        treesAvailable++;
        autoCompleters.put(node.getUUID(), auto);
        return auto;
    }

    private void cleanOldies() {
        Set<UUID> keys = autoCompleters.keySet();
        for (UUID uuid : keys) {
            AutoCompleter auto = autoCompleters.get(uuid);
            if (auto.checkLifetime() == null) {
                treesDestroyed++;
                treesAvailable--;
                autoCompleters.remove(uuid);
            }
        }
        log.info(
                    "DACK [Ava: " + treesAvailable +
                            ", Cre: " + treesCreated +
                            ", Des: " + treesDestroyed +
                            " QA: " + queriesProcessed +
                            "] @ " + new Date().toString());
    }

    public List<String> getWords(String word, UUID parentUuid, int maxCount) {
        queriesProcessed++;
        AutoCompleter auto = autoCompleters.get(parentUuid);
        if (auto == null) {
            creationPA.startIteration();
                auto = registerAutoCompleter(Sacrilege.getInstance().getAddressTree().getAddressNode(parentUuid));
            creationPA.endIteration();
        }
        queryingPA.startIteration();
            List<String> result = auto.getWords(word, maxCount);
        queryingPA.endIteration();
        cleaningPA.startIteration();
        cleanOldies();
        cleaningPA.endIteration();
        return result;
    }

}
