package ru.trollsmedjan.dao.model;

import org.hibernate.search.annotations.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

/**
 * Created by finnetrolle on 21.12.2014.
 */
@XmlRootElement
@Entity
//@Indexed
@Table(name="houses")
public class House {

    @Id
    @Column(name = "id")
    protected UUID id;

    @Column(name = "number")
//    @Field(index= Index.YES, analyze= Analyze.YES, store= Store.NO)
    protected String number;

    @Column(name = "parent")
    protected UUID parent;

    @Column(name = "postal")
    protected String postal;

    @Column(name = "buildingguid")
    protected UUID buildingUuid;

    public UUID getBuildingUuid() {
        return buildingUuid;
    }

    public void setBuildingUuid(UUID buildingUuid) {
        this.buildingUuid = buildingUuid;
    }

    public int getLevel() {
        return 100;
    }

    @Override
    public String toString() {
        return "House{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", parent=" + parent +
                ", postal='" + postal + '\'' +
                '}';
    }

    public House(UUID id, String number, UUID parent, String postal, UUID buildingUuid) {
        this.id = id;
        this.number = number;
        this.parent = parent;
        this.postal = postal;
        this.buildingUuid = buildingUuid;
    }

    public House() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public UUID getParent() {
        return parent;
    }

    public void setParent(UUID parent) {
        this.parent = parent;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String toXMLString() {
        return "<House "
                + "HOUSEID=\'" + this.id.toString() + "\' "
                + "AOGUID=\'" + this.parent.toString() + "\' "
                + "POSTALCODE=\'" + this.postal + "\' "
                + "HOUSENUM=\'" + this.number + "\'/>";
    }

    public static String generateHouseNumber(String number, String housing, String building, int strStatus) {
        StringBuilder sb = new StringBuilder().append(number);
        if (!housing.isEmpty())
            sb.append('к').append(housing);
        if (!building.isEmpty()) {
            switch (strStatus) {
                case 1: sb.append('c'); break;
                case 2: System.out.println("SOORUJENIE!!! " + number + " " + housing + " " + building); break;
                default: break;
            }
            sb.append(building);
        }
        return sb.toString();
    }
}
