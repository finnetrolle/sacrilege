package ru.trollsmedjan.dao.model.AddressTree.helpers;

import org.apache.log4j.Logger;
import ru.trollsmedjan.dao.model.AddressTree.AddressNode;
import ru.trollsmedjan.dao.model.AddressTree.AddressTree;
import ru.trollsmedjan.dao.model.AddressTree.helpers.xmlhandlers.HouseHandler;
import ru.trollsmedjan.dao.model.AddressTree.helpers.xmlhandlers.HouseProcessor;
import ru.trollsmedjan.dao.model.House;
import ru.trollsmedjan.dao.model.hibernate.BatchedHibernateWriter;
import ru.trollsmedjan.dao.model.hibernate.HibernateUtil;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

/**
 * Created by syachin on 26.12.2014.
 */
public class HouseDirectDBProcessor implements HouseProcessor {

    final static Logger log = Logger.getLogger(HouseDirectDBProcessor.class);
    private AddressTree tree = null;
    private BatchedHibernateWriter<House> writer = null;

    public HouseDirectDBProcessor(AddressTree tree) {
        this.tree = tree;
    }

    public void parseIntoDB(String filename) {
        writer = new BatchedHibernateWriter<House>(HibernateUtil.getSessionFactory(), House.class);
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = factory.newSAXParser();

            File file = new File(filename);
            if (!file.exists())
                log.error("No file for houses found");
            saxParser.parse(file, new HouseHandler(this, false));
        } catch (Exception e) {
            log.error("Parsing failed", e);
        } finally {
            writer.commit();
        }
    }

    @Override
    public boolean addHouse(House house) {
        if (tree != null) {
            AddressNode node = tree.getAddressNode(house.getParent());
            if (node == null) {
                return false;
            } else {
                node.setHasHouses(true);
            }
        }
        writer.addObject(house);
        return true;
    }
}
