// Todo cover with log4j
package ru.trollsmedjan.dao.model.AddressTree.helpers.xmlhandlers;

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import ru.trollsmedjan.dao.model.House;
import ru.trollsmedjan.dao.model.AddressTree.helpers.HouseBuilder;

import java.util.UUID;

/**
 * Created by syachin on 23.12.2014.
 */
public class HouseHandler extends DefaultHandler {

    private String uuid;
    private String parent;
    private String postal;
    private String number;
    private String housing;
    private String building;
    private String globalid;
    private int strStatus;

    private int counter = 0;
    private int allowed = 0;
    private int denied = 0;
    private boolean silent;

    private HouseProcessor houseProcessor = null;

    public HouseHandler(HouseProcessor houseProcessor, boolean silentMode) {
        super();
        this.houseProcessor = houseProcessor;
        this.silent = silentMode;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("House")) {
            uuid = attributes.getValue("HOUSEID");
            parent = attributes.getValue("AOGUID");
            postal = attributes.getValue("POSTALCODE");
            number = attributes.getValue("HOUSENUM");
            housing = attributes.getValue("BUILDNUM");
            building = attributes.getValue("STRUCNUM");
            globalid = attributes.getValue("HOUSEGUID");
            String str = attributes.getValue("STRSTATUS");
            strStatus = (str == null) ? 3 : Integer.valueOf(str);
            number = House.generateHouseNumber(number, housing, building, strStatus);


            House house = new HouseBuilder()
                    .setId(UUID.fromString(uuid))
                    .setParent(UUID.fromString(parent))
                    .setPostal((postal != null) ? postal : "")
                    .setNumber((number != null) ? number : "")
                    .setBuildingUuid((globalid != null) ? UUID.fromString(globalid) : null)
                    .createHouse();

            if (houseProcessor.addHouse(house))
                allowed++;
            else
                denied++;

            counter++;
            if ((!silent) && (counter % 100000 == 0)) {
                System.out.println("ALLOWED: " + allowed + " DENIED: " + denied + " OF " + counter );
            }
        }
    }

}
