package ru.trollsmedjan.dao.model.AddressTree.helpers;

import org.apache.log4j.Logger;
import ru.trollsmedjan.dao.model.AddressObject;
import ru.trollsmedjan.dao.model.AddressTree.AddressNode;
import ru.trollsmedjan.dao.model.AddressTree.AddressTree;
import ru.trollsmedjan.dao.model.AddressTree.helpers.xmlhandlers.AddressObjectProcessor;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by finnetrolle on 21.12.2014.
 */
public class AddressTreeBuilder implements AddressObjectProcessor {

    final static Logger log = Logger.getLogger(AddressTreeBuilder.class);
    private Map<Integer, List<AddressNode> > buffer = new HashMap<Integer, List<AddressNode>>();
    private AddressTree tree;

    public AddressTreeBuilder() {
        this.buffer = new HashMap<Integer, List<AddressNode>>();
    }

    public void addAddressObject(AddressObject addressObject) {
        // first make a node
        AddressNode node = new AddressNode(addressObject);
        // next check level
        if (!buffer.containsKey(addressObject.getLevel())) {
            List<AddressNode> list = new LinkedList<AddressNode>();
            buffer.put(addressObject.getLevel(), list);
        }
        List<AddressNode> list = buffer.get(addressObject.getLevel());
        list.add(node);
    }

    public void printBufferStatus() {
        log.info("Buffers: " + buffer.size());
        for (Map.Entry<Integer, List<AddressNode> > entry : buffer.entrySet()) {
            List<AddressNode> list = entry.getValue();
            log.info("\t" + entry.getKey() + " " + list.size());
        }
    }

    public AddressTree makeTree() {
        tree = new AddressTree();
        // pushing all addressobjects from lower level to higher
        for (int i = 0; i < 100; ++i) {
            List<AddressNode> list = buffer.get(i);
            if (list != null) {
                for (int j = list.size() - 1; j >= 0; --j) {
                    tree.addAddressNode(list.get(j));
                    list.remove(j);
                }
            }
        }
        return tree;
    }
}
