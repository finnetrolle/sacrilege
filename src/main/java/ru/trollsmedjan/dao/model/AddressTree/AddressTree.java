// Todo cover with log4j
package ru.trollsmedjan.dao.model.AddressTree;

import ru.trollsmedjan.dao.model.AddressObject;
import ru.trollsmedjan.dao.model.House;

import java.io.PrintWriter;
import java.util.*;

/**
 * Created by finnetrolle on 21.12.2014.
 */
public class AddressTree {

    private Map<UUID, AddressNode> tree;
    private Map<UUID, AddressNode> hashMap;

    public AddressTree() {
        tree = new HashMap<UUID, AddressNode>();
        hashMap = new HashMap<UUID, AddressNode>();
    }

    public Collection<AddressNode> getHashMap() {
        return hashMap.values();
    }

    public int getHashMapSize() {
        return this.hashMap.size();
    }

    public Collection<AddressNode> getRoots() {
        return tree.values();
    }

    public boolean addAddressNode(AddressNode node) {
        hashMap.put(node.getUUID(), node);
        UUID parentUuid = node.getParentUUID();

        if (parentUuid == null) {
            tree.put(node.getUUID(), node);
        } else {
            AddressNode parentNode = hashMap.get(parentUuid);
            if (parentNode != null) {
                parentNode.addChild(node);
            } else {
                return false;
            }
        }
        return true;
    }

    public boolean addAddressObject(AddressObject addressObject) {
        // create node
        AddressNode node = new AddressNode(addressObject);
        // add node
        return addAddressNode(node);
    }

    private int cleanNode(AddressNode node) {
        int result = 0;
        // kill all childs
        if (node.hasChilds()) {
            for (AddressNode child : node.getChildren()) {
                result += cleanNode(child);
            }
            node.getChildren().clear();
        }
        // suicide
        hashMap.remove(node.getUUID());
        if (node.isRoot())
            tree.remove(node.getUUID());
        return ++result;
    }

    public int cleanNodesFrom(UUID nodeUuid) {
        int result = 0;
        // search the node
        AddressNode node = hashMap.get(nodeUuid);
        if (node != null) {
            // start recursion
            result += cleanNode(node);
        }
        return result;
    }

    public AddressObject getAddressObject(UUID uuid) {
        AddressNode obj = getAddressNode(uuid);
        if (obj != null)
            return obj.getAddressObject();
        return null;
    }

    public AddressNode getAddressNode(UUID uuid) {
        if (hashMap.containsKey(uuid))
            return hashMap.get(uuid);
        return null;
    }

    public List<AddressObject> getFullAddress(UUID uuid) {
        AddressNode node = hashMap.get(uuid);
        return node.getFullAddress();
    }

    public List<AddressObject> getFullAddress(AddressObject object) {
        AddressNode node = hashMap.get(object.getUuid());
        return node.getFullAddress();
    }

}
