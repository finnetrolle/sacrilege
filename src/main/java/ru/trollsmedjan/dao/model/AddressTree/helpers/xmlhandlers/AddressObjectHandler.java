// Todo cover with log4j
package ru.trollsmedjan.dao.model.AddressTree.helpers.xmlhandlers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ru.trollsmedjan.dao.model.AddressObject;
import ru.trollsmedjan.dao.model.AddressTree.helpers.AddressObjectBuilder;

import java.util.UUID;

/**
 * Created by syachin on 23.12.2014.
 */
public class AddressObjectHandler extends DefaultHandler{

    private AddressObjectProcessor processor;
    private String uuid;
    private String parent;

    public AddressObjectHandler(AddressObjectProcessor processor) {
        this.processor = processor;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("Object")) {
            if (attributes.getValue("LIVESTATUS").equals("1")) { // write ONLY alive addresses
                uuid = attributes.getValue("AOGUID");
                parent = attributes.getValue("PARENTGUID");

                AddressObject o = new AddressObjectBuilder()
                        .setUuid(UUID.fromString(uuid))
                        .setLevel(Integer.valueOf(attributes.getValue("AOLEVEL")))
                        .setName(attributes.getValue("FORMALNAME"))
                        .setParent((parent != null) ? UUID.fromString(parent) : null)
                        .setShortName(attributes.getValue("SHORTNAME"))
                        .createAddressObject();
                processor.addAddressObject(o);

            }
        }
    }

}
