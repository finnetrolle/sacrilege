package ru.trollsmedjan.dao.model.AddressTree;

import ru.trollsmedjan.dao.model.AddressObject;
import ru.trollsmedjan.dao.model.House;

import java.util.*;

/**
 * Created by finnetrolle on 21.12.2014.
 */
public class AddressNode {

    // composite holding
    private AddressObject addressObject = null;

    // parent & childs
    private AddressNode parent = null;
    private List<AddressNode> children = null;

    public AddressObject getAddressObject() {
        return addressObject;
    }

    public UUID getUUID() {
        if (addressObject != null) return addressObject.getUuid();
        return null;
    }

    public UUID getParentUUID() {
        if (addressObject != null) return addressObject.getParent();
        return null;
    }

    public boolean hasHouses() {
        return this.addressObject.isHasHouses();
    }

    public void setHasHouses(boolean hasHouses){
        this.addressObject.setHasHouses(hasHouses);
    }

    public boolean isRoot() {
        return (parent == null);
    }

    public boolean hasChilds() {
        return (children != null);
    }

    public void addChild(AddressNode child) {
        if (children == null) {
            children = new LinkedList<AddressNode>();
        }
        children.add(child);
        child.setParent(this);
    }

    public void removeChild(AddressNode child) {
        if (children != null) {
            child.parent = null;
            children.remove(child);
        }
    }

    public int getLevel() {
        if (addressObject != null) return addressObject.getLevel();
        return -1;
    }

    public void removeChild(UUID uuid) {
        if (children == null)
            return;
        AddressNode node = null;
        for (AddressNode n : children) {
            if (n.getUUID().equals(uuid))
                node = n;
        }
        if (node != null) {
            node.parent = null;
            children.remove(node);
        }
    }

    public AddressNode(AddressObject addressObject) {
        this.addressObject = addressObject;
    }

    public void setParent(AddressNode parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        if (addressObject != null) return addressObject.toString();
        return "Empty Address Node";
    }

    public String getName() {
        if (addressObject != null) return addressObject.getName();
        return "";
    }

    public List<AddressNode> getChildren() {
        return children;
    }

    public List<AddressObject> getFullAddress() {
        List<AddressObject> nodes = new LinkedList<AddressObject>();
        this.getFullAddress(nodes);
        return nodes;
    }

    private void getFullAddress(List<AddressObject> nodes) {
        if (this.parent != null)
            this.parent.getFullAddress(nodes);
        nodes.add(this.getAddressObject());
    }

    public void getSubTreeWords(List<String> words) {
        if (children != null) {
            for (AddressNode node : children) {
                words.add(node.getName());
                node.getSubTreeWords(words);
            }
        }
    }

}
