// Todo cover with log4j
package ru.trollsmedjan.dao.model.AddressTree.helpers.xmlhandlers;

import ru.trollsmedjan.dao.model.AddressObject;

/**
 * Created by syachin on 23.12.2014.
 */
public interface AddressObjectProcessor {

    public void addAddressObject(AddressObject addressObject);
}
