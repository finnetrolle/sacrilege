package ru.trollsmedjan.dao.model.AddressTree.helpers;

import ru.trollsmedjan.dao.model.AddressObject;

import java.util.UUID;

public class AddressObjectBuilder {
    private UUID uuid;
    private String name;
    private String shortName;
    private UUID parent;
    private int level;

    public AddressObjectBuilder setUuid(UUID uuid) {
        this.uuid = uuid;
        return this;
    }

    public AddressObjectBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public AddressObjectBuilder setShortName(String shortName) {
        this.shortName = shortName;
        return this;
    }

    public AddressObjectBuilder setParent(UUID parent) {
        this.parent = parent;
        return this;
    }

    public AddressObjectBuilder setLevel(int level) {
        this.level = level;
        return this;
    }

    public AddressObject createAddressObject() {
        return new AddressObject(uuid, name, shortName, parent, level);
    }
}