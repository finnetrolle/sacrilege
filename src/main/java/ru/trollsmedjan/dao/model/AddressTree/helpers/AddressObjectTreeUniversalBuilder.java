package ru.trollsmedjan.dao.model.AddressTree.helpers;

import org.apache.log4j.Logger;
import org.xml.sax.helpers.DefaultHandler;
import ru.trollsmedjan.dao.model.AddressObject;
import ru.trollsmedjan.dao.model.AddressTree.AddressNode;
import ru.trollsmedjan.dao.model.AddressTree.AddressTree;
import ru.trollsmedjan.dao.model.AddressTree.helpers.xmlhandlers.AddressObjectHandler;
import ru.trollsmedjan.dao.model.hibernate.DAOCore;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * Created by syachin on 23.12.2014.
 */
public class AddressObjectTreeUniversalBuilder {

    final static Logger log = Logger.getLogger(AddressObjectTreeUniversalBuilder.class);
    private AddressTreeBuilder treeBuilder = new AddressTreeBuilder();
    private AddressTree tree;
    DefaultHandler handler = null;

    public AddressObjectTreeUniversalBuilder() {
        treeBuilder = new AddressTreeBuilder();
        handler = new AddressObjectHandler(treeBuilder);
    }

    public AddressObjectTreeUniversalBuilder makeTree() {
        tree = treeBuilder.makeTree();
        return this;
    }

    public AddressObjectTreeUniversalBuilder printStatus() {
        treeBuilder.printBufferStatus();
        return this;
    }

    public AddressObjectTreeUniversalBuilder cleanByAllowedList(List<String> allowedRegions) {
        log.info("Cleaning tree");
        List<UUID> keys = new LinkedList<UUID>();
        for (AddressNode node : tree.getRoots()) {
            boolean flag = true;
            String name = node.getName();
            for (String s : allowedRegions) {
                if (name.equals(s)) {
                    flag = false;
                    log.info("allowed " + name);
                }
            }
            if (flag) {
                log.info("Object " + node.toString() + " will be removed");
                keys.add(node.getUUID());
            }
        }
        for (UUID uuid : keys) {
            tree.cleanNodesFrom(uuid);
        }
        return this;
    }

    public AddressObjectTreeUniversalBuilder parse(String filename) {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = factory.newSAXParser();
            log.info("Loading AddressObjects into buffer");
            File file = new File(filename);
            saxParser.parse(file, handler);
        } catch (Exception e) {

        }
        return this;
    }

    public AddressObjectTreeUniversalBuilder loadFromDB(DAOCore dao) {
        tree = new AddressTree();
        List<AddressObject> list = dao.getAllAddressObjectsSorted();
        for (AddressObject object : list) {
            tree.addAddressObject(object);
        }
        return this;
    }

    public AddressTree build() {
        return tree;
    }
}
