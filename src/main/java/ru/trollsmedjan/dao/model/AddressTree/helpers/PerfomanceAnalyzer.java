package ru.trollsmedjan.dao.model.AddressTree.helpers;

import org.apache.log4j.Logger;

/**
 * Created by finnetrolle on 28.12.2014.
 */
public class PerfomanceAnalyzer {

    private long longest = Long.MIN_VALUE;
    private long shortest = Long.MAX_VALUE;
    private long allTime = 0;
    private long iterations = 0;
    private long iterationStartTime;
    private String name;
    final static Logger log = Logger.getLogger(PerfomanceAnalyzer.class);

    public PerfomanceAnalyzer(String name) {
        this.name = name;
    }

    public void startIteration() {
        iterationStartTime = System.currentTimeMillis();
    }

    public long endIteration() {
        long result = System.currentTimeMillis() - iterationStartTime;
        if (longest < result)
            longest = result;
        if (shortest > result)
            shortest = result;
        iterations++;
        allTime += result;
        return result;
    }

    public void endAndShout() {
        long last = endIteration();
        log.info(this.toString() + " & last = " + last);
    }

    public long getLongest() {
        return longest;
    }

    public long getShortest() {
        return shortest;
    }

    public long getAllTime() {
        return allTime;
    }

    public long getIterations() {
        return iterations;
    }

    @Override
    public String toString() {
        return name + " PA{" +
                "longest=" + longest +
                ", shortest=" + shortest +
                ", allTime=" + allTime +
                ", iterations=" + iterations +
                ", mid=" + String.valueOf(allTime / iterations) +
                '}';
    }
}
