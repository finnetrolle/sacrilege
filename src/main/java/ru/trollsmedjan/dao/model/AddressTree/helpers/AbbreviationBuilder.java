package ru.trollsmedjan.dao.model.AddressTree.helpers;

import ru.trollsmedjan.dao.model.Abbreviation;

public class AbbreviationBuilder {
    private String name;
    private int level;
    private String scName;
    private int id;

    public AbbreviationBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public AbbreviationBuilder setLevel(int level) {
        this.level = level;
        return this;
    }

    public AbbreviationBuilder setScName(String scName) {
        this.scName = scName;
        return this;
    }

    public AbbreviationBuilder setId(int id) {
        this.id = id;
        return this;
    }

    public Abbreviation createAbbreviation() {
        return new Abbreviation(name, level, scName, id);
    }
}