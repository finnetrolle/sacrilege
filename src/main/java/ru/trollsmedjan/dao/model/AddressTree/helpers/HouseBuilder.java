package ru.trollsmedjan.dao.model.AddressTree.helpers;

import ru.trollsmedjan.dao.model.House;

import java.util.UUID;

public class HouseBuilder {
    private UUID id;
    private String number;
    private UUID parent;
    private String postal;
    private UUID buildingUuid;

    public HouseBuilder setId(UUID id) {
        this.id = id;
        return this;
    }

    public HouseBuilder setNumber(String number) {
        this.number = number;
        return this;
    }

    public HouseBuilder setParent(UUID parent) {
        this.parent = parent;
        return this;
    }

    public HouseBuilder setPostal(String postal) {
        this.postal = postal;
        return this;
    }

    public HouseBuilder setBuildingUuid(UUID buildingUuid) {
        this.buildingUuid = buildingUuid;
        return this;
    }

    public House createHouse() {
        return new House(id, number, parent, postal, buildingUuid);
    }
}