package ru.trollsmedjan.dao.model.SimpleGeometry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by syachin on 19.01.2015.
 */
@Entity
@XmlRootElement
public class Resulter {
    @Id
    @Column(name = "id")
    private Long id;
    @Column(name="name")
    private String name;
    @Column(name="lvl")
    private String lvl;
    @Column(name="geometry")
    private String geometry;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLvl() {
        return lvl;
    }

    public void setLvl(String lvl) {
        this.lvl = lvl;
    }

    public String getGeometry() {
        return geometry;
    }

    public void setGeometry(String geometry) {
        this.geometry = geometry;
    }
}
