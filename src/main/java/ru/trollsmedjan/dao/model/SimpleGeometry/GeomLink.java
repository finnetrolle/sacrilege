package ru.trollsmedjan.dao.model.SimpleGeometry;

import org.hibernate.search.annotations.Indexed;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

/**
 * Created by syachin on 19.01.2015.
 */
@Entity
@Indexed
@Table(name="GeomLink")
@XmlRootElement
public class GeomLink {

    @Id
    @Column(name = "UUID")
    private UUID objectId;

    @Column(name = "OSMID")
    private long osmId;

    public UUID getObjectId() {
        return objectId;
    }

    public void setObjectId(UUID objectId) {
        this.objectId = objectId;
    }

    public long getOsmId() {
        return osmId;
    }

    public void setOsmId(long osmId) {
        this.osmId = osmId;
    }
}
