package ru.trollsmedjan.dao.model.SimpleGeometry;

import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by syachin on 19.01.2015.
 */
@Entity
@Indexed
@Table(name="testlsettlementpolygon")
@XmlRootElement
public class DBSettlement {

    @Id
    @Column(name = "osm_id")
    private long osmId;

    @Column(name = "name")
    private String name;

    @Column(name = "gid")
    private int gid;

    @Column(name ="name_en")
    private String nameEn;

    @Column(name ="name_ru")
    private String nameRu;

    @Column(name = "place")
    private String place;

    @Column(name = "a_cntr")
    private String aCntr;

    @Column(name = "a_rgn")
    private String aRgn;

    @Column(name = "a_dstrct")
    private String aDstrct;

    @Column(name = "a_pstcd")
    private String aPostCode;

    @Column(name = "population")
    private String pop;

    @Column(name = "geom")
    private Geometry geometry;

    public long getOsmId() {
        return osmId;
    }

    public void setOsmId(long osmId) {
        this.osmId = osmId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getaCntr() {
        return aCntr;
    }

    public void setaCntr(String aCntr) {
        this.aCntr = aCntr;
    }

    public String getaRgn() {
        return aRgn;
    }

    public void setaRgn(String aRgn) {
        this.aRgn = aRgn;
    }

    public String getaDstrct() {
        return aDstrct;
    }

    public void setaDstrct(String aDstrct) {
        this.aDstrct = aDstrct;
    }

    public String getaPostCode() {
        return aPostCode;
    }

    public void setaPostCode(String aPostCode) {
        this.aPostCode = aPostCode;
    }

    public String getPop() {
        return pop;
    }

    public void setPop(String pop) {
        this.pop = pop;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }
}
