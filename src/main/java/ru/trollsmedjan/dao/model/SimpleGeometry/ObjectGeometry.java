package ru.trollsmedjan.dao.model.SimpleGeometry;


import com.vividsolutions.jts.algorithm.CGAlgorithms;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.operation.overlay.PolygonBuilder;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.UUID;

/**
 * Created by syachin on 14.01.2015.
 */
@Entity
@Indexed
@Table(name="ObjectGeometry")
@XmlRootElement
public class ObjectGeometry {


    @Column(name = "geometry")
    private Polygon geometry;

    @Id
    @Column(name = "id", unique = true)
    private long id;

    public Envelope getEnvelope() {
        return geometry.getEnvelopeInternal();
    }

    public Point getCentroid() {
        return geometry.getCentroid();
    }

    public Polygon getGeometry() {
        return geometry;
    }

    public void setGeometry(Polygon geometry) {
        this.geometry = geometry;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public static ObjectGeometry create(Long id, Coordinate[] coordinates) {
        GeometryFactory gf = new GeometryFactory();
        coordinates[coordinates.length-1] = coordinates[0];
        LinearRing ring = gf.createLinearRing(coordinates);
        Polygon poly = new Polygon(ring, null, gf);
        ObjectGeometry obj = new ObjectGeometry();
        obj.setGeometry(poly);
        obj.setId(id);
        return obj;
    }
}
