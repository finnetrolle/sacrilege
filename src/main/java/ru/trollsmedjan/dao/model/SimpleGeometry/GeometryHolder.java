package ru.trollsmedjan.dao.model.SimpleGeometry;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.index.strtree.SIRtree;
import com.vividsolutions.jts.index.strtree.STRtree;

import java.util.*;

/**
 * Created by syachin on 14.01.2015.
 */
public class GeometryHolder {

    private STRtree tree = new STRtree();
    private Map<Long, ObjectGeometry> hash = new HashMap<Long, ObjectGeometry>();

    public void put(ObjectGeometry geometry) {
        tree.insert(geometry.getEnvelope(), geometry);
        hash.put(geometry.getId(), geometry);
    }

    public List<ObjectGeometry> get(Envelope envelope) {
        return tree.query(envelope);
    }

    public List<ObjectGeometry> get(Coordinate point) {
        Envelope env = new Envelope(point, point);
        return tree.query(env);
    }

    public ObjectGeometry get(Long id) {
        if (hash.containsKey(id))
            return hash.get(id);
        return null;
    }

    public void fillWithTestData() {
        tree = new STRtree();
        hash = new HashMap<Long, ObjectGeometry>();

        double dx = 0;
        double dy = 0;

        Random rand = new Random();
long start = System.currentTimeMillis();
        for (int i = 0; i < 100000; ++i) {
            dx = 60.0 + 10 * rand.nextDouble();
            dy = 30.0 + 10 * rand.nextDouble();
            Coordinate[] coords = new Coordinate[50];
            for (int z = 0; z < 50; ++z) {
                coords[z] = new Coordinate(dx + rand.nextDouble(), dy + rand.nextDouble());
            }
            ObjectGeometry o = ObjectGeometry.create((long)i, coords);
            this.put(o);
        }
long end = System.currentTimeMillis();
        System.out.println("Creating: " + (end - start));
        List<ObjectGeometry> l = tree.query(new Envelope(60,60,30,30));
long firstQuery = System.currentTimeMillis();
        System.out.println("First query: " + (firstQuery - end));
        l = tree.query(new Envelope(60,60,30,30));
long secondQuery = System.currentTimeMillis();
        System.out.println("Second query: " + (secondQuery - firstQuery));

        List<ObjectGeometry> list = tree.query(new Envelope(61.0, 62.0, 30.0, 31.0));
        System.out.println("query result: " + list.size());

        System.out.println("Hello!");
    }

}
