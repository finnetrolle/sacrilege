package ru.trollsmedjan.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by finnetrolle on 20.12.2014.
 */
@Entity
@Table(name="abbreviations")
public class Abbreviation {

    @Column(name="name")
    private String name;

    @Column(name="level")
    private int level;

    @Column(name="scName")
    private String scName;

    @Id
    @Column(name="id")
    private int id;

    public Abbreviation() {
    }

    public Abbreviation(String name, int level, String scName, int id) {
        this.name = name;
        this.level = level;
        this.scName = scName;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getScName() {
        return scName;
    }

    public void setScName(String scName) {
        this.scName = scName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
