package ru.trollsmedjan.dao.model.hibernate;


import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.io.WKTReader;
import org.apache.log4j.Logger;
import org.apache.lucene.search.*;
import org.hibernate.Session;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import ru.trollsmedjan.dao.model.AddressObject;
import ru.trollsmedjan.dao.model.AddressTree.AddressNode;
import ru.trollsmedjan.dao.model.AddressTree.AddressTree;
import ru.trollsmedjan.dao.model.House;
import ru.trollsmedjan.dao.model.SimpleGeometry.Resulter;

import javax.sound.sampled.Line;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * Created by syachin on 23.12.2014.
 */
public class DAOCore {

    public static final int DEFAULT_MAXIMUM_RESULTS_COUNT = 50;
    public static final int SYSTEM_MAXIMUM_FTS_RESULTS_COUNT = 1000;
    final static Logger log = Logger.getLogger(DAOCore.class);

    public List<AddressObject> getAddressObject(String fullName) {
        log.info("using public List<AddressObject> getAddressObject(String fullName)");
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<AddressObject> result = session
                .createQuery("from AddressObject WHERE name = :name")
                .setParameter("name", fullName)
                .list();
        return result;
    }

    public AddressObject getAddressObject(UUID uuid) {
        log.info("using public AddressObject getAddressObject(UUID uuid)");
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<AddressObject> list = session
                .createQuery("from AddressObject WHERE uuid = :uuid")
                .setParameter("uuid", uuid)
                .list();
        return list.get(0);
    }

    public AddressObject getAddressObjectLazy(UUID uuid) {
        log.info("using public AddressObject getAddressObjectLazy(UUID uuid)");
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (AddressObject)session.load(AddressObject.class, uuid);
    }

    public List<AddressObject> getAddressObjectFTS(String fullName) {
        log.info("using public List<AddressObject> getAddressObjectFTS(String fullName)");
        Session session = HibernateUtil.getSessionFactory().openSession();
        FullTextSession fullTextSession = Search.getFullTextSession(session);

        QueryBuilder queryBuilder = fullTextSession
                .getSearchFactory()
                .buildQueryBuilder()
                .forEntity(AddressObject.class)
                .get();

        org.apache.lucene.search.Query luceneQuery = queryBuilder
                .keyword()
                .onFields("name")
                .matching(fullName)
                .createQuery();
        FullTextQuery fullTextQuery = fullTextSession.createFullTextQuery(luceneQuery, AddressObject.class);
        List<AddressObject> result = fullTextQuery.list();
        fullTextSession.close();
        return result;
    }


    public List<AddressObject> getAllAddressObjectsSorted() {
        log.info("using public List<AddressObject> getAllAddressObjectsSorted()");
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session.createQuery("from AddressObject order by level").list();
    }

    public List<House> getHouses(UUID parentUuid) {
        log.info("using public List<House> getHouses(UUID parentUuid)");
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session
                .createQuery("from House WHERE parent = :parent ORDER BY number")
                .setParameter("parent", parentUuid)
                .list();
    }

    public List<House> getHouses(String word, UUID parentUuid) {
        log.info("using public List<House> getHouses(String word, UUID parentUuid)");
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session
                .createQuery("from House WHERE parent = :parent AND number = :word ORDER BY number")
                .setParameter("parent", parentUuid)
                .setParameter("word", word)
                .list();
    }

    public List<House> getHouses(List<String> words, UUID parentUuid) {
        log.info("public List<House> getHouses(List<String> words, UUID parentUuid)");
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session
                .createQuery("from House WHERE parent = :parent AND number in :word ORDER BY number")
                .setParameter("parent", parentUuid)
                .setParameterList("word", words)
                .list();
    }

    /**
     * Search for address objects MATCH fullname and EQUALS parent = parentUUID
     * e.g. all objects with name contains 'Mira' and parent equals parentUUID (in town)
     * @param fullName - word to MATCH name field
     * @param parentUuid - word to EQUALS parent UUID
     * @param maxResultsCount - maximum results
     * @return list of resulting objects
     */
    public List<AddressObject> getAddressObjectFTS(String fullName, UUID parentUuid, int maxResultsCount) {
        log.info("using public List<AddressObject> getAddressObjectFTS(String fullName, UUID parentUuid, int maxResultsCount)");
        // Create session for hibernate query
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            // Create Full Text Search session
            FullTextSession fullTextSession = Search.getFullTextSession(session);
            QueryBuilder queryBuilder = fullTextSession
                    .getSearchFactory()
                    .buildQueryBuilder()
                    .forEntity(AddressObject.class)
                    .get();

            Query luceneQuery = null;
            if (parentUuid == null) {
                luceneQuery = queryBuilder
                        .keyword()
                        .onField("name")
                        .matching(fullName)
                        .createQuery();
            } else {
                luceneQuery = queryBuilder
                        .bool()
                        .must(queryBuilder.keyword().onField("name").matching(fullName).createQuery())
                        .must(queryBuilder.keyword().onField("parent").matching(parentUuid).createQuery())
                        .createQuery();
            }


            FullTextQuery fullTextQuery = fullTextSession.createFullTextQuery(luceneQuery, AddressObject.class);

            // limit result size to 100
            fullTextQuery.setMaxResults(maxResultsCount);
            // enable sorting by level
            Sort sort = new Sort(new SortField("level", SortField.Type.INT));
            fullTextQuery.setSort(sort);
            // return result
            return fullTextQuery.list();
        }
        catch (Exception e) {
            log.error(e.getMessage());
            log.error(e.toString());
            return new LinkedList<AddressObject>();
        }
    }

    public void makeIndexes() {
        log.info("making indexes ");
        Session session = HibernateUtil.getSessionFactory().openSession();
        FullTextSession fullTextSession = Search.getFullTextSession(session);
        try {
            fullTextSession.createIndexer().startAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void injectAddressTree(AddressTree tree) {
        log.info("injecting AddressTree ");
        BatchedHibernateWriter<AddressObject> writer =
                new BatchedHibernateWriter<AddressObject>(HibernateUtil.getSessionFactory(),AddressObject.class);
        for (AddressNode node : tree.getHashMap()) {
            AddressObject obj = node.getAddressObject();
            writer.addObject(obj);
        }
        writer.commit();
    }

    public void getGIS() {
        log.info("using public List<House> getHouses(UUID parentUuid)");
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Object> list = session.createQuery("from planet_osm_polygon").list();

//        return session
//                .createQuery("from House WHERE parent = :parent ORDER BY number")
//                .setParameter("parent", parentUuid)
//                .list();
    }

    private List<Resulter> loadBoundaries() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Resulter> list =  session.createSQLQuery("SELECT \n" +
                "\tosm_id AS id,\n" +
                "\tname AS name,\n" +
                "\tadmin_lvl AS lvl,\n" +
                "\tST_ASEWKT(geom) as geometry\n" +
                "FROM\n" +
                "\t\"l-boundary-polygon\"").addEntity(Resulter.class).list();
        list.addAll(session.createSQLQuery("SELECT \n" +
                "\tosm_id AS id,\n" +
                "\tname AS name,\n" +
                "\tadmin_lvl AS lvl,\n" +
                "\tST_ASEWKT(geom) as geometry\n" +
                "FROM\n" +
                "\t\"s-boundary-polygon\"").addEntity(Resulter.class).list());
        return list;
    }
    private List<Resulter> loadSettlements() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Resulter> list = session.createSQLQuery("SELECT \n" +
                "\tosm_id AS id,\n" +
                "\tname AS name,\n" +
                "\tplace AS lvl,\n" +
                "\tST_ASEWKT(geom) AS geometry\n" +
                "FROM\n" +
                "\t\"l-settlement-polygon\"").addEntity(Resulter.class).list();
        list.addAll(session.createSQLQuery("SELECT \n" +
                "\tosm_id AS id,\n" +
                "\tname AS name,\n" +
                "\tplace AS lvl,\n" +
                "\tST_ASEWKT(geom) AS geometry\n" +
                "FROM\n" +
                "\t\"s-settlement-polygon\"").addEntity(Resulter.class).list());
        return list;
    }



    public void getSettlements() {
        WKTReader reader = new WKTReader();
        List<Resulter> list = loadBoundaries();
        for (Resulter r : list) {
            System.out.println("BOUNDARY " + r.getId() + " : " + r.getName() + " with " + r.getGeometry());
            try {
                Geometry g = reader.read(r.getGeometry());
                System.out.println(g.toString());
            } catch (Exception e) {
                System.out.println("ASSHOLE!!!");
            }
        }
//        list = loadSettlements();
//        for (Resulter r : list) {
//            System.out.println("SETTLEMENT " + r.getId() + " : " + r.getName() + " with " + r.getGeometry());
//        }
    }

}
