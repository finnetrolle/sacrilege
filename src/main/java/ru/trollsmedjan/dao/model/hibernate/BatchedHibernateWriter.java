package ru.trollsmedjan.dao.model.hibernate;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by finnetrolle on 20.12.2014.
 */
public class BatchedHibernateWriter<T> {

    private final int BUFFER_SIZE = 50;
    private final int SESSION_SIZE = 200;
    private int counter;
    private int flushCounter;
    private int sessionCounter;
    private int linesInjected;
    private Session session;
    private Transaction tx;
    private List<T> objs = new LinkedList<T>();
    private final Class<T> clazz;
    private final SessionFactory sessionFactory;
    final static Logger log = Logger.getLogger(BatchedHibernateWriter.class);

    public BatchedHibernateWriter(SessionFactory sessionFactory, Class<T> clazz) {
        this.sessionFactory = sessionFactory;
        this.clazz = clazz;
        this.sessionCounter = 0;
        this.linesInjected = 0;
        openSession();
    }

    private void openSession() {
        session = sessionFactory.openSession();
        tx = session.beginTransaction();
        counter = 0;
        flushCounter = 0;
        sessionCounter++;
        log.info("[" + new Date().toString() + "]" + clazz.getCanonicalName() + ": session " + sessionCounter + " opened");
    }

    public int getInjectedLinesCount() {
        return this.linesInjected;
    }

    private void closeSession() {
        tx.commit();
        session.close();
        log.info("[" + new Date().toString() + "]" + clazz.getCanonicalName() + ": session " + sessionCounter + " closed");
    }

    public void commit() {
        closeSession();
    }

    private void flush() {
        if (flushCounter >= SESSION_SIZE) {
            closeSession();
            openSession();
            flushCounter = 0;
        } else {
            flushCounter ++;
            session.flush();
            session.clear();
        }
    }

    public void addObject(T obj) {
        if (counter >= BUFFER_SIZE) {
            flush();
            counter = 0;
        }
        counter++;
        session.saveOrUpdate(obj);
//        session.save(obj);
        linesInjected ++;
    }

}
