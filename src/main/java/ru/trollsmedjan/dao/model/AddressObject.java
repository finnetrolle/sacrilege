package ru.trollsmedjan.dao.model;

import org.hibernate.search.annotations.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

/**
 * Created by finnetrolle on 20.12.2014.
 */
@Entity
@Indexed
@Table(name="AddressObjects")
@XmlRootElement
public class AddressObject {

    @Id
    @Column(name = "uuid", unique = true)
    protected UUID uuid;

    @Column(name="name")
    @Field(index= Index.YES, analyze= Analyze.YES, store= Store.NO)
    protected String name;

    @Column(name="shortName")
    protected String shortName;

    @Column(name = "parent")
    @Field(index= Index.YES, analyze= Analyze.NO, store= Store.NO)
    protected UUID parent;

    @Column(name = "level")
    @Field(index= Index.YES, analyze= Analyze.NO, store= Store.NO)
    protected int level;

    @Column(name = "hasHouses")
    @Field(index = Index.YES, analyze = Analyze.NO, store = Store.NO)
    protected boolean hasHouses;

    public boolean isHasHouses() {
        return hasHouses;
    }

    public void setHasHouses(boolean hasHouses) {
        this.hasHouses = hasHouses;
    }

    public AddressObject() {
        this.hasHouses = false;
    }

    public AddressObject(UUID uuid, String name, String shortName, UUID parent, int level) {
        this.uuid = uuid;
        this.name = name;
        this.shortName = shortName;
        this.parent = parent;
        this.level = level;
        this.hasHouses = false;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public UUID getParent() {
        return parent;
    }

    public void setParent(UUID parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return "AddressObject{" +
                "uuid=" + uuid +
                ", name='" + name + '\'' +
                ", shortName='" + shortName + '\'' +
                ", parent=" + parent +
                ", level=" + level +
                '}';
    }

    public String toXMLString() {
        String p = (parent != null) ? ("PARENTGUID=\'" + this.parent.toString() + "\' ") : "";
        return "<Object "
             + "AOGUID=\'" + this.uuid.toString() + "\' "
             + "FORMALNAME=\'" + this.name + "\' "
             + "LIVESTATUS=\'1\' "
             + p
             + "AOLEVEL=\'" + this.level + "\' "
             + "SHORTNAME=\'" + this.shortName.toString() + "\'/>";
    }


}
