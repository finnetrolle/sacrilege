package ru.trollsmedjan.sacrilege.responseobjects;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by syachin on 13.01.2015.
 */
@XmlRootElement
public class ResponseSmartFind {
    private String queryWord;
    private String zeroWord;
    private List<ResponseFullAddress> zeroResults;
    private List<ResponseFullAddress> otherResults;
    private List<String> autocompletes;

    public List<ResponseFullAddress> getOtherResults() {
        return otherResults;
    }

    public void setOtherResults(List<ResponseFullAddress> otherResults) {
        this.otherResults = otherResults;
    }

    public String getQueryWord() {
        return queryWord;
    }

    public void setQueryWord(String queryWord) {
        this.queryWord = queryWord;
    }

    public String getZeroWord() {
        return zeroWord;
    }

    public void setZeroWord(String zeroWord) {
        this.zeroWord = zeroWord;
    }

    public List<ResponseFullAddress> getZeroResults() {
        return zeroResults;
    }

    public void setZeroResults(List<ResponseFullAddress> zeroResults) {
        this.zeroResults = zeroResults;
    }

    public List<String> getAutocompletes() {
        return autocompletes;
    }

    public void setAutocompletes(List<String> autocompletes) {
        this.autocompletes = autocompletes;
    }
}
