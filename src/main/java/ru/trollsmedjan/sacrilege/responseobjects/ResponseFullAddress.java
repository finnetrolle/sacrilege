package ru.trollsmedjan.sacrilege.responseobjects;

import ru.trollsmedjan.dao.model.AddressObject;
import ru.trollsmedjan.dao.model.House;
import ru.trollsmedjan.sacrilege.Sacrilege;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.UUID;

/**
 * Created by syachin on 23.12.2014.
 */
@XmlRootElement
public class ResponseFullAddress {

    House house;
    List<AddressObject> fullAddress;

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    public List<AddressObject> getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(List<AddressObject> fullAddress) {
        this.fullAddress = fullAddress;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (AddressObject o : fullAddress) {
            builder.append(o.getShortName()).append(" ").append(o.getName()).append(", ");
        }
        if (house != null) {
            builder.append(house.getNumber());
        }
        return builder.toString();
    }

    public static ResponseFullAddress build(House house) {
        ResponseFullAddress rfa = new ResponseFullAddress();
        rfa.setHouse(house);
        rfa.setFullAddress(Sacrilege.getInstance().getAddressTree().getFullAddress(house.getParent()));
        return rfa;
    }

    public static ResponseFullAddress build(AddressObject object, UUID parent) {
        ResponseFullAddress rfa = new ResponseFullAddress();
        rfa.setHouse(null);
        rfa.setFullAddress(Sacrilege.getInstance().getAddressTree().getFullAddress(object.getUuid()));
        boolean flag = true;
        if (parent != null) {
            flag = false;
            for (AddressObject obj : rfa.getFullAddress()) {
                if (obj.getUuid().equals(parent))
                    flag = true;
            }
        }
        if (flag)
            return rfa;
        return null;
    }

    public boolean isEqualTo(ResponseFullAddress other) {
        if ((this.getHouse() != null) && (other.getHouse() != null)) {
            if (this.getHouse().getId() != other.getHouse().getId())
                return false;
        } else {
            if (other.getHouse() != null)
                return false;
        }
        // houses is equal
        AddressObject thisLast = this.fullAddress.get(this.fullAddress.size()-1);
        AddressObject otherLast = other.fullAddress.get(other.fullAddress.size()-1);
        if (thisLast.getUuid() != otherLast.getUuid())
            return false;
        return true;
    }
}
