package ru.trollsmedjan.sacrilege;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import ru.trollsmedjan.dao.model.AddressTree.helpers.AddressObjectTreeUniversalBuilder;
import ru.trollsmedjan.dao.model.AddressTree.AddressNode;
import ru.trollsmedjan.dao.model.AddressTree.AddressTree;
import ru.trollsmedjan.dao.model.AddressTree.helpers.HouseDirectDBProcessor;
import ru.trollsmedjan.dao.model.SimpleGeometry.GeometryHolder;
import ru.trollsmedjan.dao.model.autocompleter.AutoCompleter;
import ru.trollsmedjan.dao.model.autocompleter.DynamicAutoCompleterKernel;
import ru.trollsmedjan.dao.model.hibernate.DAOCore;
import ru.trollsmedjan.dao.model.hibernate.HibernateUtil;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by finnetrolle on 19.12.2014.
 */
public class Sacrilege {

    private static volatile Sacrilege instance;

    private AddressTree addressTree = null;
    private AutoCompleter autoComplete = null;
    private DAOCore dao = null;
    private DynamicAutoCompleterKernel dack = null;
    private GeometryHolder geo = null;

    final static Logger log = Logger.getLogger(Sacrilege.class);

    private Sacrilege() {

    }

    public GeometryHolder getGeo() {
        return geo;
    }

    public void inject(String addressObjectsFilename, String housesFilename) {
        log.info("Injecting XML files into database");
        // allowed regions list
        // Todo remove this hardcode
        List<String> allowed = new LinkedList<String>();
        allowed.add("Санкт-Петербург");
        allowed.add("Ленинградская");

        dao = new DAOCore();

        // load addressTree
        try {
            addressTree = new AddressObjectTreeUniversalBuilder()
                    .parse(addressObjectsFilename)
                    .printStatus()
                    .makeTree()
                    .cleanByAllowedList(allowed)
                    .printStatus()
                    .build();
        } catch (Exception e) {
            log.error("AddressTree loading failed", e);
        }

        // load & inject houses
        try {
            HouseDirectDBProcessor processor = new HouseDirectDBProcessor(addressTree);
            processor.parseIntoDB(housesFilename);
        } catch (Exception e) {
            log.error("Houses injection failed", e);
        }

        // inject addressObjects
        try {
            dao.injectAddressTree(addressTree);
        } catch (Exception e) {
            log.error("AddressTree injection failed", e);
        }

        // make indexes
        try {
            dao.makeIndexes();
        } catch (Exception e) {
            log.error("Indexing failed", e);
        }

        log.info("Injection is overed");
    }

    public void init() {
        log.info("Loading data from database");
        // init services
        dao = new DAOCore();
        dack = new DynamicAutoCompleterKernel().setSilentMode(true);
        // load addressobjects from db
        addressTree = new AddressObjectTreeUniversalBuilder()
                .loadFromDB(dao)
                .build();
        // prepare autocompleter
        autoComplete = new AutoCompleter();
        for (AddressNode node : addressTree.getHashMap()) {
            autoComplete.addWord(node.getName());
        }
        // geo
        geo = new GeometryHolder();
        geo.fillWithTestData();
        dao.getSettlements();
        log.info("Autocompleter contains: " + autoComplete.getLoaded());
    }

    public DynamicAutoCompleterKernel getDack() {
        return dack;
    }

    public DAOCore getDao() {
        return dao;
    }

    public AutoCompleter getAutoCompleter() {
        return autoComplete;
    }

    public AddressTree getAddressTree() {
        return this.addressTree;
    }

    public static Sacrilege getInstance() {
        if (instance == null)
            synchronized (Sacrilege.class) {
                if (instance == null) {
                    instance = new Sacrilege();
                    log.info("Sacrilege initialized");
                }
            }
        return instance;
    }

    public static String getFullPath(String filename) {
        String fname = null;
        try {
            fname = new File(".").getCanonicalPath() + File.separator + filename;
            File f = new File(fname);
            if (f.exists()) {
                log.info("File " + fname + " is found");
            } else {
                log.error("File " + fname + "is not found");
                fname = null;
            }
        } catch (Exception e) {
            log.error("Full path getting is failed. Gratz!", e);
        } finally {
            return fname;
        }
    }

    public static void main(String[] args) throws Exception{
        boolean pInject = false;

        for (String s : args) {
            if (s.equals("inject"))
                pInject = true;
        }

        // there are 2 modes - injecting db and working as service
        if (pInject) {
            // inject and exit
            Sacrilege.getInstance().inject(getFullPath("OUT_AddressObjects.XML"), getFullPath("OUT_Houses.XML"));
             HibernateUtil.getSessionFactory().close();
        } else {
            Sacrilege.getInstance().init();

            log.info("Starting web server");
            ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
            context.setContextPath("/");
            Server jettyServer = new Server(8080);
            jettyServer.setHandler(context);

            ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
            jerseyServlet.setInitOrder(0);
            jerseyServlet.setInitParameter("jersey.config.server.provider.classnames", JsonTest.class.getCanonicalName());
            jerseyServlet.setInitParameter("jersey.config.server.provider.packages", "org.codehaus.jackson.jaxrs");

            try {
                jettyServer.start();
                jettyServer.join();
            } finally {
                jettyServer.destroy();
            }
        }

    }

}
