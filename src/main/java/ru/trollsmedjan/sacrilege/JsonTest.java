package ru.trollsmedjan.sacrilege;

import org.apache.log4j.Logger;
import ru.trollsmedjan.dao.model.AddressObject;
import ru.trollsmedjan.dao.model.AddressTree.helpers.PerfomanceAnalyzer;
import ru.trollsmedjan.dao.model.House;
import ru.trollsmedjan.dao.model.hibernate.DAOCore;
import ru.trollsmedjan.sacrilege.responseobjects.ResponseFullAddress;
import ru.trollsmedjan.sacrilege.responseobjects.ResponseSmartFind;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.*;

/**
 * Created by finnetrolle on 20.12.2014.
 */

@Path("/json")
@Produces("application/json; charset=UTF-8")
@Consumes("application/json")
public class JsonTest {

    final static Logger log = Logger.getLogger(JsonTest.class);
    static int counter = 0;
    private static PerfomanceAnalyzer jsonPA = new PerfomanceAnalyzer("JsonTest");

    /**
     * Метод оборачивает список объектов в список пригодных для отдачи полных адресов
     * @param objects список адресных объектов
     * @return список адресных цепочек
     */
    private static List<ResponseFullAddress> getResponseFullAddressList(List<AddressObject> objects) {
        List<ResponseFullAddress> result = new LinkedList<ResponseFullAddress>();
        for (AddressObject obj : objects) {
            ResponseFullAddress rfa = new ResponseFullAddress();
            rfa.setHouse(null);
            rfa.setFullAddress(Sacrilege.getInstance().getAddressTree().getFullAddress(obj));
            result.add(rfa);
        }
        return result;
    }

    private static List<ResponseFullAddress> getResponseFullAddressList(List<House> houses, List<AddressObject> fullPath) {
        List<ResponseFullAddress> result = new LinkedList<ResponseFullAddress>();
        for (House house : houses) {
            ResponseFullAddress rfa = new ResponseFullAddress();
            rfa.setHouse(house);
            rfa.setFullAddress(fullPath);
            result.add(rfa);
        }
        return result;
    }

    /**
     * Метод оборачивает объект в выходной формат с указанием заголовков для кросс-доменного запроса
     * @param object объект на выход
     * @return готовый к отправке объект с заголовками, разрешающими кросс-доменный запрос
     */
    private static Response getCorrectXDomainResponse(Object object) {
        counter++;
        return Response.ok().entity(object)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .allow("OPTIONS").build();
    }

    // Todo add logger

    @GET
    @Path("findAddresses")
    public Response findAddresses(
            @QueryParam("word") String word,
            @QueryParam("parentguid") String lastAddressGuid,
            @QueryParam("limit") Integer maxCount) {
        log.info("findAddress query ?w=" + word + "&p=" + lastAddressGuid + "&l=" + maxCount);
        jsonPA.startIteration();
        // first correct arguments
        int max = (maxCount != null) ? maxCount : Sacrilege.getInstance().getDao().DEFAULT_MAXIMUM_RESULTS_COUNT;
        UUID parent = (lastAddressGuid != null) ? UUID.fromString(lastAddressGuid) : null;
        String query = (word != null) ? word : "";
        List<ResponseFullAddress> result = getAddressess(word, parent, max);
//        List<ResponseFullAddress> result = new LinkedList<ResponseFullAddress>();
//
//        // add address objects
//        addToResponse(result,
//                Sacrilege.getInstance().getDao().getAddressObjectFTS(
//                        word, null, DAOCore.SYSTEM_MAXIMUM_FTS_RESULTS_COUNT), parent);
//
//        // add houses
//        if ((parent != null) && (Sacrilege.getInstance().getAddressTree().getAddressNode(parent).hasHouses())) {
//            addToResponse(result,
//                    Sacrilege.getInstance().getDao().getHouses(word.toUpperCase(), parent));
//        }
        jsonPA.endAndShout();
        return getCorrectXDomainResponse(result);
    }

    private List<ResponseFullAddress> getAddressess(String word, UUID parent, int max) {
        List<ResponseFullAddress> result = new LinkedList<ResponseFullAddress>();
        addToResponse(result, Sacrilege.getInstance().getDao().getAddressObjectFTS(
                word, null, DAOCore.SYSTEM_MAXIMUM_FTS_RESULTS_COUNT), parent);
        if ((parent != null) && (Sacrilege.getInstance().getAddressTree().getAddressNode(parent).hasHouses())) {
            addToResponse(result,
                    Sacrilege.getInstance().getDao().getHouses(word, parent));
        }
        return result;
    }

    private List<ResponseFullAddress> getAddressess(List<String> words, UUID parent, int max) {
        List<ResponseFullAddress> result = new LinkedList<ResponseFullAddress>();
        StringBuilder sb = new StringBuilder();
        for (String s : words) {
            sb.append(s);
            sb.append(" ");
        }
        addToResponse(result, Sacrilege.getInstance().getDao().getAddressObjectFTS(
                sb.toString(), null, DAOCore.SYSTEM_MAXIMUM_FTS_RESULTS_COUNT), parent);
        if ((parent != null) && (Sacrilege.getInstance().getAddressTree().getAddressNode(parent).hasHouses())) {
            addToResponse(result,
                    Sacrilege.getInstance().getDao().getHouses(words, parent));
        }
        return result;
    }

    private static void addToResponse(List<ResponseFullAddress> result, List<House> houses) {
        for (House house : houses)
            result.add(ResponseFullAddress.build(house));
    }

    private static void addToResponse(List<ResponseFullAddress> result, List<AddressObject> objects, UUID parent) {
        for (AddressObject obj : objects) {
            ResponseFullAddress rfa = ResponseFullAddress.build(obj, parent);
            if (rfa != null)
                result.add(rfa);
        }
    }


    /**
     * Метод возвращает список адресных цепочек. В случае наличия в запросе GUID последнего объекта цепочки,
     * возвращаются только те объекты, которые принадлежат этому родителю (только прямое наследование)
     * @param word полное слово (или несколько слов через пробел) для поиска
     * @param lastAddressGuid GUID объекта, в котором будет происходить поиск
     * @param maxCount максимальный размер массива результатов
     * @return массив адресных цепочек
     */
    @GET
    @Path("findAddressObjects")
    public Response findAddressObjects(
            @QueryParam("word") String word,
            @QueryParam("parentguid") String lastAddressGuid,
            @QueryParam("limit") Integer maxCount) {
        log.info("findAddressObjects query ?w=" + word + "&p=" + lastAddressGuid + "&l=" + maxCount);
        jsonPA.startIteration();
        // correct arguments
        int max = (maxCount != null) ? maxCount : Sacrilege.getInstance().getDao().DEFAULT_MAXIMUM_RESULTS_COUNT;
        UUID parent = (lastAddressGuid != null) ? UUID.fromString(lastAddressGuid) : null;
        String query = (word != null) ? word : "";
        // do query
        // understand if we have houses to load
        boolean hasHouses = false;
        if (parent != null)
            hasHouses = Sacrilege.getInstance().getAddressTree().getAddressObject(parent).isHasHouses();
        // if we have houses - look for word in houses
        if (hasHouses) {
            List<House> houses = Sacrilege.getInstance().getDao().getHouses(word, parent);
            List<AddressObject> fullPath = Sacrilege.getInstance().getAddressTree().getFullAddress(parent);
            List<AddressObject> list = Sacrilege.getInstance().getDao().getAddressObjectFTS(query, parent, max);
            List<ResponseFullAddress> result = getResponseFullAddressList(list);
            result.addAll(getResponseFullAddressList(houses, fullPath));
            jsonPA.endAndShout();
            return getCorrectXDomainResponse(result);
        } else {
            List<AddressObject> list = Sacrilege.getInstance().getDao().getAddressObjectFTS(query, parent, max);
            jsonPA.endAndShout();
            return getCorrectXDomainResponse(getResponseFullAddressList(list));
        }
    }




    /**
     * Метод возвращает массив строк, пригодных для подстановки по слову word. В случае ненулевого GUID поиск будет
     * проходить только среди дочерних элементов объекта с этим GUID
     * @param word первые буквы слова для автоподстановки
     * @param lastAddressGuid GUID объекта, в котором будет происходить поиск
     * @param maxCount максимальный размер массива результатов
     * @return массив строк для автоподстановки
     */
    @GET
    @Path("autocomplete")
    public Response autocomplete(
            @QueryParam("word") String word,
            @QueryParam("parentguid") String lastAddressGuid,
            @QueryParam("limit") Integer maxCount) {
        log.info("autocomplete query ?w=" + word + "&p=" + lastAddressGuid + "&l=" + maxCount);
        jsonPA.startIteration();
        // correct arguments
        int max = (maxCount != null) ? maxCount : Sacrilege.getInstance().getDao().DEFAULT_MAXIMUM_RESULTS_COUNT;
        UUID parent = (lastAddressGuid != null) ? UUID.fromString(lastAddressGuid) : null;
        String query = (word != null) ? word.toLowerCase() : "";
        List<String> result = getAutocompleteResult(query, parent, max);
//        if (parent == null) {
//            // simple query to root autocompleter
//            result = Sacrilege.getInstance().getAutoCompleter().getWords(query, max);
//        } else {
//            result = Sacrilege.getInstance().getDack().getWords(query, parent, max);
//        }
        jsonPA.endAndShout();
        return getCorrectXDomainResponse(result);
    }

    @GET
    @Path("smartFind")
    public Response smartFind(
            @QueryParam("word") String word,
            @QueryParam("parentguid") String lastAddressGuid,
            @QueryParam("limit") Integer maxCount) {
        log.info("smartFind query ?w=" + word + "&p=" + lastAddressGuid + "&l=" + maxCount);
        jsonPA.startIteration();
        // correct arguments
        int max = (maxCount != null) ? maxCount : Sacrilege.getInstance().getDao().DEFAULT_MAXIMUM_RESULTS_COUNT;
        UUID parent = (lastAddressGuid != null) ? UUID.fromString(lastAddressGuid) : null;
        String query = (word != null) ? word.toLowerCase() : "";
        // first check autocomplete results
        List<String> acr = getAutocompleteResult(query, parent, max);
        ResponseSmartFind result = new ResponseSmartFind();
        result.setQueryWord(query);
        result.setAutocompletes(acr);
        result.setOtherResults(new LinkedList<ResponseFullAddress>());
        if (!acr.isEmpty()) {
            result.setZeroWord(acr.get(0));
            result.setZeroResults(getAddressess(result.getZeroWord(), parent, max));

            int midMax = max - result.getZeroResults().size();

            if (acr.size() == 1) {

            }
            if (acr.size() == 2) {
                List<ResponseFullAddress> list = getAddressess(acr.get(1), parent, max);
                addOtherResults(list, result.getOtherResults(), midMax);
            }
            if (acr.size() > 2) {
                List<String> findList = new LinkedList<String>();
                for (int i = 1; i < acr.size(); ++i) {
                    findList.add(acr.get(i));
                }
                List<ResponseFullAddress> list = getAddressess(findList, parent, max);
                addOtherResults(list, result.getOtherResults(), midMax);
            }


        }
        else {
            result.setZeroWord("");
            result.setZeroResults(new LinkedList<ResponseFullAddress>());
        }

        jsonPA.endIteration();
        return getCorrectXDomainResponse(result);
    }

    private void addOtherResults(List<ResponseFullAddress> source, List<ResponseFullAddress> dest, int max) {
        for (ResponseFullAddress a : source) {
            if (dest.size() > max)
                return;
            if (!isFullAddressInList(a, dest)) {
                dest.add(a);
            }
        }
    }

    private boolean isFullAddressInList(ResponseFullAddress address, List<ResponseFullAddress> list) {
        for (ResponseFullAddress a : list) {
            // check houses
            if (a.isEqualTo(address))
                return true;
        }
        return false;
    }

    private List<String> getAutocompleteResult(String word, UUID parent, int maxCount) {
        if (parent == null) {
            return Sacrilege.getInstance().getAutoCompleter().getWords(word, maxCount);
        } else {
            return Sacrilege.getInstance().getDack().getWords(word, parent, maxCount);
        }
    }

}
